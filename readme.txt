=== GoUrl Bitcoin Altcoin Payment Gateway For Gravity Forms ===
Contributors: mohsinoffline
Donate link: https://wpgateways.com/support/send-payment/
Tags: Gravity Forms bitcoin, bitcoin, altcoins, cryptocurrency, payment gateway, Gravity Forms, Gravity Forms payment gateway, payment forms, bitcoin cash, dash, litecoin
Plugin URI: https://wpgateways.com/products/gourl-bitcoin-altcoin-gateway-gravity-forms/
Author URI: https://wpgateways.com
Requires at least: 4.0
Tested up to: 5.3
Stable tag: 1.0.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin enables you to use the GoUrl.io payment gateway and accept bitcoin and other altcoins directly on your Gravity Forms powered custom forms via GoUrl payment box.

== Description ==

[GoUrl.io](https://gourl.io/) Payment Gateway allows you to accept cryptocurrency payments from all over the world on your websites and deposit funds automatically into your external wallets. They support 13 different cryptocurrencies including Bitcoin, Bitcoin Cash, Dash and Litecoin.

[Gravity Forms](https://www.gravityforms.com/) is one of the oldest most advanced custom form plugins in the WordPress sphere. Powering over a million websites, Gravity Forms allows you to quickly and easily integrate with a variety of third party services with a fantastic collection of add-ons.

Here's a list of what the plugin provides out of the box with the help of core GoUrl.io plugin:

* **Open Source**: 100% free Open Source plugin on [Bitbucket](https://bitbucket.org/gateways/gf-gourl).
* **Secure Cryptocurrency Processing**: Securely process crypto coins without redirecting your customers to the gateway website. No need to worry about chargebacks.
* **Multiple Coin Support**: Supports payments in Bitcoin, BitcoinCash, Litecoin, Dash, Dogecoin, Speedcoin, Reddcoin, Potcoin, Feathercoin, Vertcoin, Peercoin, and MonetaryUnit.
* **Easy Withdrawals**: Get crypto payments straight to your bitcoin/altcoin wallet addresses. Use your exchange wallet address for withdrawals and convert to fiat.
* **Conditional Payments**: Supports conditional statements in Gravity Forms.
* **Paid Posts**: Optionally lets you charge for posts by letting you publish them in Gravity Forms only after the payment is complete.
* **Logging**: Enable logging so you can debug issues that arise if any.

**Requirements**
-------
* [Gravity Forms](https://www.gravityforms.com/) plugin 2.0 or later.
* [GoUrl Core](https://wordpress.org/plugins/gourl-bitcoin-payment-gateway-paid-downloads-membership/) WordPress Plugin.
* [GoUrl.io](https://gourl.io/) account with payment boxes setup.

**Extend, Contribute, Integrate**
-------

Visit the [plugin page](https://wpgateways.com/products/gourl-bitcoin-altcoin-gateway-gravity-forms/) for more details. Contributors are welcome to send pull requests via [Bitbucket repository](https://bitbucket.org/gateways/gf-gourl/).

For custom integration with your WordPress website, please [contact us here](https://wpgateways.com/support/custom-payment-gateway-integration/).

Disclaimer: This plugin is not affiliated with or supported by Gravity Forms, GoUrl.io or any cryptocurrency developer or founder team. All logos and trademarks are the property of their respective owners.

== Installation ==

1. Install the core GoUrl.io plugin from [this link](https://wordpress.org/plugins/gourl-bitcoin-payment-gateway-paid-downloads-membership/).
2. Register on [GoUrl.io](https://gourl.io/), create payment boxes for each cryptocurrency you want to charge in and finish setting up the GoUrl Plugin settings as mentioned (sections 1 and 2) under the installation instructions on their [plugin page](https://wordpress.org/plugins/gourl-bitcoin-payment-gateway-paid-downloads-membership/#installation).
3. Upload `gravityforms-gourl` folder/directory to the `/wp-content/plugins/` directory
4. Activate the plugin (Wordpress -> Plugins).
5. Go to the Gravity Forms settings page (Wordpress -> Forms -> Settings) and set the forms currency to a cryptocurrency if you so wish. This is optional and if you choose to leave it as a fiat currency like USD, the plugin will automatically do the conversion for you.
6. Now go to the forms page (Wordpress -> Forms) and create/edit a form.
7. Now from under the "Form Settings" drop down menu above the form editor, select "GoUrl (BTC/Alts)" and click on "Add New".
8. Select "Products and Services" as your transaction type, configure the payment box settings and select the field to pick the amount from.
9. Click on "Update Settings". That's it! 
10. You can use 500 free Speedcoins or Dogecoins for testing. URL: https://speedcoin.org/info/free_coins/Free_Speedcoins.html

You are ready to accept crypto payments using the GoUrl payment gateway now connected to Gravity Forms.


== Changelog ==

= 1.0.4 =

* Updated donation link

= 1.0.3 =

* Tested with the latest version of WordPress (5.3)

= 1.0.2 =

* Added payment box to the payment confirmation page
* Fixed fatal error upon activation in case the GoUrl base plugin is missing
* Fixed notices and a warning

= 1.0.1 =

* Added exchange rate multiplier option
* Fixed default user setting

= 1.0.0 =

* Initial release version